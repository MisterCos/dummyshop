import {
  createReducer,
  on
} from '@ngrx/store';
import { login, logout } from './auth.actions';
import { User } from '../../core/models/User';




export const authFeatureKey = 'auth';

export interface AuthState {
  user:User | undefined
}

export const initialState: AuthState = {
  user: undefined
}

// export const reducers: ActionReducerMap<AuthState> = {

// };

export const authReducer = createReducer(
  initialState,
  on(login, (state, action) => {
    return{
      user:action.user
    }
  }),
  on(logout, () => {
    return{
      user: undefined
    }
  })
);

