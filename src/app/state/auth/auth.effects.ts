import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";

import { tap } from "rxjs/operators";

import { Router } from "@angular/router";
import { login, logout } from "./auth.actions";


@Injectable()
export class AuthEffects{


  login$ = createEffect(() => this.action$.pipe(
    ofType(login),
    tap(action => {
      localStorage.setItem('user',JSON.stringify(action.user))
    })
  ),{dispatch:false});


  logout$ = createEffect(() => this.action$.pipe(
    ofType(logout),
    tap(action => {
      localStorage.removeItem('user')
      this.router.navigateByUrl('/login')
    }),
  ),{dispatch:false});

  constructor(private action$:Actions,private router:Router) {

    // action$.subscribe(action => {
    //   if(action.type === '[Login Page] User Login') {
    //     localStorage.setItem('user',JSON.stringify(action['user']));
    //   }
    // });
  }

}
