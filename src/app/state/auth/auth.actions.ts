import { createAction, props } from "@ngrx/store";
import { User } from "../../core/models/User";


// action creation
export const login = createAction(
  "[Login Page] User Login",
  props<{user:User}>()
)

export const logout = createAction(
  '[Top Menu] Logout'
)
