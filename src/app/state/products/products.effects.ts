import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";

import { concatMap, map } from "rxjs/operators";
import { allProductsLoaded, loadAllProducts } from "./products.actions";
import { productsAdapter } from "../../core/adapters/productsAdapter";
import { ProductService } from "../../core/services/product.service";



@Injectable()
export class ProductsEffects{


  loadProducts$ = createEffect(() => this.actions$.pipe(
    ofType(loadAllProducts),
    concatMap(() => this.productService.getAllProducts()),
    map(products => {
      const adaptedProducts = productsAdapter(products)
      console.log(adaptedProducts)
      return allProductsLoaded({products:adaptedProducts})
    })
  )
)

  constructor(private actions$:Actions,private productService:ProductService) {}

}
