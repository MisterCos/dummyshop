import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ProductState, adapter } from "./products.reducers";
import { Product } from "../../core/models/Product";


export const selectProductsState = createFeatureSelector<ProductState>('products');

export const selectAllProducts = createSelector(
  selectProductsState,
  adapter.getSelectors().selectAll);

  export const areProductsLoaded = createSelector(
    selectProductsState,
    state => state.allProductsLoaded
  );

  export const selectFavoriteProductsCount = createSelector(
    selectAllProducts,
    (products: Product[]) => products.filter(product => product.isFavorite).length
  );


  export const selectFavoriteProducts = createSelector(
    selectAllProducts,
    (products: Product[]) => products.filter(product => product.isFavorite)
  );
