import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { createReducer, on } from "@ngrx/store";

import { editFavoritesProducts, allProductsLoaded, removeAllProducts } from "./products.actions";
import { Product } from "../../core/models/Product";

export interface ProductState extends EntityState<Product> {
  allProductsLoaded: boolean;
}

export const adapter = createEntityAdapter<Product>({

});

export const initialCoursesState = adapter.getInitialState({
  allProductsLoaded: false
});

export const productsReducer = createReducer(
  initialCoursesState,
  on(allProductsLoaded, (state, action) =>
    adapter.setAll(action.products, {...state, allProductsLoaded: true})
  ),
  on( editFavoritesProducts, (state, action) =>
    adapter.updateOne(action.update, state)
  ),
  on(removeAllProducts, (state,action) =>
    adapter.removeAll(initialCoursesState)
  )

);


