import { createAction, props } from "@ngrx/store";
import { Update } from "@ngrx/entity";
import { Product } from "../../core/models/Product";


export const loadAllProducts = createAction('[Products Resolver] Load All Products');

export const allProductsLoaded = createAction('[Load Products Effect] All Products Loaded',props<{products:Product[]}>());

export const editFavoritesProducts=createAction('[Product List] Edit Favorite Product',props<{update:Update<Product>}>());

export const removeAllProducts=createAction('[Product List] Remove All Products]');
