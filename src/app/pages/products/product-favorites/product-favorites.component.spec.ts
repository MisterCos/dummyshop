import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import { AppState } from "../../../state/app";
import { editFavoritesProducts } from "../../../state/products/products.actions";
import { ProductFavoritesComponent } from "./product-favorites.component";
import { Update } from "@ngrx/entity";
import { Product } from "../../../core/models/Product";

describe('ProductFavoritesComponent', () => {
  const mockProduct = {
    id: 1,
    thumbnail: 'https://example.com/thumbnail.jpg',
    title: 'Mock Product',
    description: 'This is a mock product',
    price: 99.99,
    rating: 5,
    category: 'Mock Category',
    isFavorite: true
  };
  let component: ProductFavoritesComponent;
  let fixture: ComponentFixture<ProductFavoritesComponent>;

  let mockStore = {
    select: jest.fn(),
    dispatch: jest.fn()
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductFavoritesComponent],
      providers: [
        { provide: Store, useValue: mockStore }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductFavoritesComponent);
    component = fixture.componentInstance;


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select favorite products on init', () => {
    const products = [mockProduct];
    jest.spyOn(mockStore, 'select').mockReturnValue(of(products));
    component.ngOnInit();
    expect(component.products).toEqual(products);
  });

  it('should increment pendingUpdatesArray ', () => {
    const product = mockProduct;
    component.removeFavorite(product);

    expect(component.pendingUpdates.length).toBe(1);
  });

  it('should change heart icon when favorite status is toggled', () => {
    const product =mockProduct ;
    component.products = [product];
    component.changeHeartIcon(product.id, !product.isFavorite);
    expect(component.products[0].isFavorite).toBe(!product.isFavorite);
  });

  it('should dispatch updates to store on destroy', () => {
    const product = mockProduct;
    component.pendingUpdates = [{
      id: product.id,
      changes: { ...product, isFavorite: !product.isFavorite }
    }];

    const update = component.pendingUpdates[0];
    component.ngOnDestroy();

    expect(mockStore.dispatch).toHaveBeenCalledWith(editFavoritesProducts({ update }));
    expect(component.pendingUpdates).toEqual([]);
  });
});
