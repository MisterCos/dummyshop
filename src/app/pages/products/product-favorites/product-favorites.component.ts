import { Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from '../../../core/models/Product';
import { Store } from '@ngrx/store';
import { AppState } from '../../../state/app';
import { selectFavoriteProducts } from '../../../state/products/products.selectors';
import { editFavoritesProducts } from '../../../state/products/products.actions';
import { first } from 'rxjs';

@Component({
  selector: 'app-product-favorites',
  templateUrl: './product-favorites.component.html',
  styleUrl: './product-favorites.component.scss'
})
export class ProductFavoritesComponent implements OnInit,OnDestroy
{

  products:Product[] = [];
  pendingUpdates: { id: number, changes: Product }[] = [];
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
      this.store.select(selectFavoriteProducts).subscribe(products => {
        this.products = products;
      })
  }

  removeFavorite(product: Product) {

    // Hacemos una copia del objeto producto ya que este esta como readOnly
    const updatedProduct = { ...product, isFavorite: !product.isFavorite };

    const update = {
      id: updatedProduct.id,
      changes: updatedProduct
    }

    this.changeHeartIcon(updatedProduct.id, updatedProduct.isFavorite);
    // Guardamos los cambios en el array de pendientes
    this.pendingUpdates.push(update);
  }

  changeHeartIcon(id: number, isFavorite: boolean) {
    const productIndex = this.products.findIndex(p => p.id === id);
    if (productIndex > -1) {
      this.products[productIndex] = { ...this.products[productIndex], isFavorite };
    }
  }

  // Aplicamos los cambios en el destroy ( en este caso cuando se cambia la pagina)
  ngOnDestroy(): void {
    this.pendingUpdates.forEach(update => {
      this.store.dispatch(editFavoritesProducts({ update }));
    });
    this.pendingUpdates = [];
  }

}
