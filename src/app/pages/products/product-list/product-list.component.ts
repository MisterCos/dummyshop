import { Component, OnInit } from '@angular/core';
import { Update } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { Product } from '../../../core/models/Product';
import { AppState } from '../../../state/app';
import { editFavoritesProducts } from '../../../state/products/products.actions';
import { selectAllProducts } from '../../../state/products/products.selectors';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.scss'
})
export class ProductListComponent implements OnInit{


  products: Product[] = []
  constructor( private store: Store<AppState>) {

  }

  ngOnInit() {

    this.store.select(selectAllProducts).subscribe(products => {
      this.products = products;
    })
  }
  addProductToFavorites(product: Product) {

    const update:Update<Product> = {
      id: product.id,
      changes: {
        ...product,
        isFavorite: !product.isFavorite
      }
    }
    this.store.dispatch(editFavoritesProducts({update}));
  }

}
