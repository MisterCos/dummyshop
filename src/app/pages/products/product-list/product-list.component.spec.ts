

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ProductListComponent } from './product-list.component';
import { ProductService } from '../../../core/services/product.service';
import { Store } from '@ngrx/store';
import { Product } from '../../../core/models/Product';
import { Update } from '@ngrx/entity';
import { editFavoritesProducts } from '../../../state/products/products.actions'
import { DataViewModule } from 'primeng/dataview';

describe('ProductListComponent', () => {


  const mockProduct = {
    id: 1,
    thumbnail: 'https://example.com/thumbnail.jpg',
    title: 'Mock Product',
    description: 'This is a mock product',
    price: 99.99,
    rating: 5,
    category: 'Mock Category',
    isFavorite: false
  }

  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;
  let mockProductService = {
    getProducts: jest.fn(),
  };
  let mockStore = {
    select: jest.fn(),
    dispatch: jest.fn()
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductListComponent],
      providers: [

        { provide: ProductService, useValue: mockProductService },
        { provide: Store, useValue: mockStore }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductListComponent);

    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize products$', () => {
    const products: Product[] = [mockProduct];
    mockStore.select.mockReturnValue(of(products));

    component.ngOnInit();

    expect(component.products).toEqual(products);
  });

  it('should dispatch addProductToFavorites action', () => {
    const product: Product = mockProduct;
    const update: Update<Product> = {
      id: product.id,
      changes: {
        ...product,
        isFavorite: !product.isFavorite
      }
    };

    component.addProductToFavorites(product);

    expect(mockStore.dispatch).toHaveBeenCalledWith(editFavoritesProducts({ update }));
  });
});
