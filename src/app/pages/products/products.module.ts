import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DataViewModule } from 'primeng/dataview';
import { FormsModule } from '@angular/forms';
import { RatingModule } from 'primeng/rating';
import { ButtonModule } from 'primeng/button';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../../shared/shared.module';
import { ProductsEffects } from '../../state/products/products.effects';
import { productsReducer } from '../../state/products/products.reducers';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsResolver } from '../../core/resolvers/products.resolver';
import { Router, RouterModule, Routes } from '@angular/router';
import { ProductFavoritesComponent } from './product-favorites/product-favorites.component';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { authInterceptor } from '../../core/interceptors/auth.interceptor';

const routes: Routes = [
  {
    path: '',
    component: ProductListComponent,
    resolve: {
      products: ProductsResolver
    },
  },
  {
    path: 'favorites',
    component: ProductFavoritesComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];



const primengModules = [DataViewModule,RatingModule,ButtonModule]

@NgModule({
  declarations: [
    ProductListComponent,
    ProductFavoritesComponent
  ],
  imports: [

    SharedModule,
    CommonModule,
    FormsModule,
    ...primengModules,
    EffectsModule.forFeature([ProductsEffects]),
    StoreModule.forFeature('products',productsReducer),
    RouterModule.forChild(routes)
  ],
  providers:[
    ProductsResolver,
    provideHttpClient(withInterceptors([authInterceptor])),
  ],
  exports:[ProductListComponent,ProductFavoritesComponent]
})
export class ProductsModule { }
