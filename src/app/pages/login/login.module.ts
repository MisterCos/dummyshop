import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ToastModule } from 'primeng/toast';
import * as fromAuth from '../../state/auth/auth.reducers';
import { LoginComponent } from './login.component';

import { isLoggedGuard } from '../../core/guards/isLogged.guard';
import { AuthEffects } from '../../state/auth/auth.effects';
const primengModules = [
  PasswordModule,
  CheckboxModule,
  InputTextModule,
  ButtonModule,
  ToastModule,
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...primengModules,
    RouterModule,
    HttpClientModule,

    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  exports: [LoginComponent],
  providers: [MessageService,isLoggedGuard],
})
export class LoginPageModule {}
