// FILEPATH: /home/marcos/programacion/pruebas/dummyShop/src/app/pages/login/login.component.spec.ts

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { LoginComponent } from './login.component';

import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthService } from '../../core/services/auth.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockAuthService = {
    login: jest.fn(),
  };
  let mockRouter= {
    navigateByUrl: jest.fn()
  };
  let mockStore= {
    dispatch: jest.fn()
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [LoginComponent],
      providers: [
        { provide: AuthService, useValue: mockAuthService },
        { provide: Router, useValue: mockRouter },
        { provide: Store, useValue: mockStore }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call AuthService.login on form submit', () => {
    const user = { username: 'test', password: 'test' };
    mockAuthService.login.mockReturnValue(of(user));
    component.loginFormGroup.setValue(user);

    component.onSubmit();

    expect(mockAuthService.login).toHaveBeenCalledWith(user.username, user.password);
    expect(mockStore.dispatch).toHaveBeenCalled();
    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/home');
  });
});
