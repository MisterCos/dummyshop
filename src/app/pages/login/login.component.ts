import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';
import { login } from '../../state/auth/auth.actions';
import { AppState } from '../../state/app';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  isSubmitted = false;
  authError = false;
  authMessage = 'Email or Password are wrong';

  loginFormGroup: FormGroup = new FormGroup({
    username: new FormControl('atuny0', [Validators.required]),
    password: new FormControl('9uQFF1Lh', Validators.required),
  });

  constructor( private store:Store<AppState>,
    private authService: AuthService,
    private router:Router,
    private messageService: MessageService,
  ) {

  }

  onSubmit() {
    const { username, password } = this.loginFormGroup.value;
    this.isSubmitted = true;
    this.authError = false;
    this.authService.login(username,password).subscribe(
      {
        next: (user) => {
          this.store.dispatch(login({user}))
          this.router.navigateByUrl('/home')
        },
        error: () => {
          this.messageService.add({severity:'error', summary:'Error', detail:'Email or Password are wrong'});
        }
      }

    )
  }
  get loginForm() {
    return this.loginFormGroup.controls;
  }
}
