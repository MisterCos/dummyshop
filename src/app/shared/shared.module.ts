import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { InputTextModule } from 'primeng/inputtext';
import { BadgeModule } from 'primeng/badge';
import { StyleClassModule } from 'primeng/styleclass';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
const primengModules =[
  ButtonModule,
    RippleModule,
    BadgeModule,
    InputTextModule,
    StyleClassModule,

]

@NgModule({
  declarations: [
    HeaderComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ...primengModules,
  ],
  exports: [HeaderComponent]
})
export class SharedModule { }
