import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { userName } from '../../../state/auth/auth.selectors';
import { logout } from '../../../state/auth/auth.actions';
import { selectFavoriteProductsCount } from '../../../state/products/products.selectors';
import { AppState } from '../../../state/app';
import { removeAllProducts } from '../../../state/products/products.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit{

userName$:Observable<string | undefined>;
favoritesProductsCount$:Observable<number> | undefined;


  constructor(private store:Store<AppState>) {
    this.userName$ = this.store.select(userName)
    this.favoritesProductsCount$ = this.store.select(selectFavoriteProductsCount)
  }

ngOnInit(): void {

}

logout(){
  this.store.dispatch(logout());
  this.store.dispatch(removeAllProducts())
}
}
