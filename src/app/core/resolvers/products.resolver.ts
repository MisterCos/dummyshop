import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { filter, finalize, first, tap } from 'rxjs';
import { areProductsLoaded } from '../../state/products/products.selectors';
import { loadAllProducts } from '../../state/products/products.actions';
import { AppState } from '../../state/app';


@Injectable()
export class ProductsResolver implements Resolve<any> {
  loading = false;
  constructor(private store: Store<AppState>) {}

  resolve() {
    return this.store.pipe(

      // we use this in order to only fetch the data once and not every time we navigate to the page
      select(areProductsLoaded),
      tap((productsLoaded) => {
        if (!this.loading && !productsLoaded) {
          this.loading = true;
          this.store.dispatch(loadAllProducts());
        }
      }),
      filter((productsLoaded) => productsLoaded),
      first(),
      finalize(() => {
        this.loading = false;
      })
    );
  }
}
