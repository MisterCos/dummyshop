export interface Product {
  id: number;
  thumbnail: string;
  title: string;
  description: string;
  price: number;
  rating: number;
  category: string;
  isFavorite: boolean;
}
