import { HttpInterceptorFn } from '@angular/common/http';
import { environment } from '../../../environments/environment';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const user = JSON.parse(localStorage.getItem('user')||'{}');
  const isApiUrl = req.url.startsWith(environment.baseUrl + '/auth/products');
  console.log('isApiUrl',isApiUrl,user.token,req.method);
  if(user.token && isApiUrl){
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${user.token}`,
        ContentType: 'application/json'
      }
    });
  }
  return next(req);
};
