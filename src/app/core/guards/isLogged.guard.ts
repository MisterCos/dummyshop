import { Inject, Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterOutlet,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { Store, select } from "@ngrx/store";
import { tap } from "rxjs/operators";
import { isLoggedIn } from "../../state/auth/auth.selectors";
import { AppState } from "../../state/app";



@Injectable()
export class isLoggedGuard implements CanActivate {
  constructor(private store:Store<AppState>,private router: Router){}
  canActivate(
  ): Observable<boolean >
     {
      return this.store.pipe(
        select(isLoggedIn),
        tap(loggedIn => {
          if (loggedIn) {
            this.router.navigateByUrl('/home');
            return false

          }
          return true
      } ))
    }

}
