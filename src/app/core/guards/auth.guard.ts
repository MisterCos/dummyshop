import { Injectable, inject } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateFn,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { AppState } from "../../state/app";
import { isLoggedIn } from "../../state/auth/auth.selectors";


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private store:Store<AppState>,private router: Router){}
  canActivate(
  ): Observable<boolean >
     {
      return this.store.pipe(
        select(isLoggedIn),
        tap(loggedIn => {
          if (!loggedIn) {
            this.router.navigateByUrl('/login');
            return false;
          }else{
            return true;
          }
      } ))
    }

}
