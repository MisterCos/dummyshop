import { Product } from "../models/Product";

 export function productsAdapter(data: any): Product[]{
  return data.products.map((product: any) => {
    return {
      id: product.id,
      thumbnail: product.thumbnail,
      title: product.title,
      description: product.description,
      price: product.price,
      rating: product.rating,
      category: product.category,
      isFavorite: false
    }
  })
 }
