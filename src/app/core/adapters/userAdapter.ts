import { User } from "../models/User";

 export function userAdapter(data: any): User{
  return {
    id: data.id,
    userName: data.username,
    token: data.token
  }
 }
