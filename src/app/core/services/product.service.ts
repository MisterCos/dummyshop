import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { Product } from "../models/Product";

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  productApiUrl = `${environment.baseUrl}/auth/products`;
  constructor(private http: HttpClient) {}

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productApiUrl)
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(`${this.productApiUrl}/${product.id}`, product)
  }

  deleteProduct(id: number): Observable<Product> {
    return this.http.delete<Product>(`${this.productApiUrl}/${id}`)
  }

}
