import { Observable, map } from "rxjs";
import { User } from "../models/User";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { userAdapter } from "../adapters/userAdapter";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.baseUrl}/auth/login`, JSON.stringify({
      username,
      password,
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    }).pipe(
      map((data: any) => userAdapter(data) )
    );
  }


}
