import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { login } from './state/auth/auth.actions';
import { AppState } from './state/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit{
  title = 'dummyShop';

  constructor(private store:Store<AppState>) {}

  ngOnInit(): void {
      const userProfiles = localStorage.getItem('user');
      if(userProfiles){
        this.store.dispatch(login({user:JSON.parse(userProfiles)}))
    }
  }


}
