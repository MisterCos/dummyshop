# DummyShop


## Resumen


Prueba técnica para la empresa zetes, en este caso he intentado aplicar las buenas prácticas de angular y seguir el patrón redux.


Al final he podido seguir todos los requisitos de la prueba incluyendo la parte de favoritos. Lo único que me he saltado es NO usar librerías de componentes, en mi caso he usado [primeng](https://primeng.org/)


En este prueba he usado numerosas características de angular tales como


 - **Guards**: en este caso han sido para proteger la ruta de home impidiendo que el usuario entre sin estar logueado
 - **Interceptors**: usados para añadir el token del login para poder hacer el resto de peticiones sin tener que estar seteando los header uno por uno
 - **Resolver**: he optado por usar un resolver para hacer un fetch de todo el listado de productos. En este caso además he guardado un flag de allProductsLoaded en ngrx para solo hacer una llamada a la api, permitiéndonos poder movernos por la app sin hacer constantes peticiones al back
 - **Servicios**: en cuanto a la comunicación con el back he usado los servicios de angular para poder traer los datos de la api




Respecto a la librería de **NGRX** He optado por crear una carpeta state en el root de la aplicación y replicar el scaffolding de la app. Teniendo dentro de la misma cada estado separado por "features" y dentro de cada carpeta los reducers, effects, selectors etc.
> Ngrx nunca lo he usado para trabajar asi que espero no haber hecho ninguna burrada con la librería 😁


Como cosas interesantes he creado dentro de la carpeta CORE una carpeta de adapters, en este caso lo he usado para coger solo los datos que nos interesaban tanto del usuario como del producto acorde a su modelo creado también en esa carpeta


También tenemos una carpeta shared en la que he metido un componente layout encargado de ser el wrapper de toda la app y un componente header.


Como cosas a mejorar, el componente de product-list y el de products-favorites usan el mismo html, se podría sacar ese componente a uno nuevo llamado product-item para no repetir. He usado metodos deprecados en el guard, ya que han cambiado tambien en angular 17 y no he tenido tiempo en exceso de mirarlos. Por ultimo no compruebo que el token que guardo en el localstorage haya expirado.




## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
